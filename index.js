import path from 'path';
import dotenv from 'dotenv';
import { ENV } from './src/common/contsants/env';

import App from './src/App';

const options = !ENV.IS_DEV() && {
	path: path.resolve(process.cwd(), '.production.env')
};

dotenv.config(options);

App();
