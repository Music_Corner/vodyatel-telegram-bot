import TelegramBot from 'node-telegram-bot-api';

let bot;

export const initBot = () => {
	try {
		if (!bot) {
			bot = new TelegramBot(process.env.TELEGRAM_TOKEN, { polling: true });
		}

		return bot;
	} catch (e) {
		console.error(e);
	}
};
