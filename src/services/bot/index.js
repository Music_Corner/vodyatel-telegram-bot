import { logger } from '../logger';
import { initBot } from './initBot';

export const sendBotMessage = async (chatId, text, options = {}) => {
	try {
		const parse_mode = options.parse_mode === undefined ? 'Markdown' : options.parse_mode;
		logger.info(`sennding a message for for ${chatId} with text: ${text}`, chatId, text);

		return await initBot().sendMessage(chatId, text, { ...options, parse_mode });
	} catch (error) {
		logger.error('error in sendBotMessage() func', error);
	}
};

export const editBotMessageText = async (text, options = {}, callBackQueryId) => {
	try {
		const parse_mode = options.parse_mode === undefined ? 'Markdown' : options.parse_mode;
		logger.info('editing a message', options);

		await initBot().editMessageText(text, { ...options, parse_mode });
		await initBot().answerCallbackQuery(callBackQueryId);
	} catch (error) {
		logger.error('error in editBotMessage() func', error);
	}
};
