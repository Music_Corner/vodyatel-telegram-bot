import moment from 'moment';

import { EMOJIS } from '../../../../common/contsants/emojis';
import { ENV } from '../../../../common/contsants/env';
import { NAVIGATION } from '../../../../common/contsants/scenarios';
import { parseCarNumber, parseRate } from '../../../../common/helpers';
import getMongoConnection from '../../../mongodb';

export const getReviewText = ({
	carNumber, text, rate,
	userName, locationLink, createdAt,
}, showCarNumber = false) => {
	const carNumberText = showCarNumber ? `<strong>Гос номер авто</strong>: ${carNumber}\n` : '';

	const locationFooter = locationLink ? `\n<strong>Местоположение</strong>: <a href="${locationLink}">Посмотреть на карте</a>` : '';
	const userNameFooter = userName ? `\n\n<strong>Автор</strong>: @${userName}` : '';
	const createdAtFooter = createdAt ? `\n<strong>Дата отзыва</strong>: ${moment(createdAt).format('DD MMM YYYY')}` : '';

	return `${carNumberText}${text}\n\n<strong>Рейтинг</strong>: ${parseRate(rate)}/5${locationFooter}${userNameFooter}${createdAtFooter}`;
};

export const getEmojiNumber = (number) => {
	const strNumber = `${number}`;

	return strNumber.split('').reduce((accum, val) => `${accum}${EMOJIS[val]}`, '');
};

export const getReviewsText = (reviews = [], page = 0, showEveryCarNumber = false) => {
	const parsedCarNumber = reviews.length ? parseCarNumber(reviews[0].carNumber) : '';
	const currentIndexByPage = ENV.DEFAULT_LIMIT * page;
	const accum = showEveryCarNumber ? '' : `<strong>Отзывы по ${parsedCarNumber}</strong>:`;

	return `${reviews.reduce((_accum, value, index) => `${_accum}\n\n\n${getEmojiNumber(index + 1 + currentIndexByPage)} ${getReviewText(value, showEveryCarNumber)}`, accum)}\n\n\n${NAVIGATION.MAIN_HTML_FORMAT}`;
};

export const getChatsCollections = async () => {
	const connection = await getMongoConnection();
	const collections = connection.db('chatsdb').collection('chats');

	return collections;
};

export const getChatInstance = async (id) => {
	const chatsCollection = await getChatsCollections();

	const chat = await chatsCollection.findOne({ chatId: id }) || {};

	return chat;
};

export const getReview = async (id) => {
	const {
		stage,
		reviewCarNumber: carNumber = '',
		reviewText: text = '',
		reviewRate: rate = '',
		reviewLocation: location = null,
		reviewLocationLink: locationLink = '',
		reviewUserName: userName = '',
	} = await getChatInstance(id);

	return {
		stage,
		carNumber,
		text,
		rate,
		location,
		locationLink,
		userName,
	};
};
