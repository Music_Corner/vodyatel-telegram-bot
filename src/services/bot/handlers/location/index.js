import { sendBotMessage } from '../..';
import { ADD_REVIEW_FORM_KEYBOARD, QUERY_DATA, REMOVE_LOCATION_BUTTON } from '../../../../common/contsants/scenarios';
import { getLocationLink } from '../../../../common/helpers';
import { getChatInstance, getChatsCollections, getReview, getReviewText } from '../common';

export const handleBotLocation = async (message) => {
	const { location, chat: { id } } = message;
	const { stage	} = await getChatInstance(id);

	const sendMessage = sendBotMessage.bind(null, id);

	const collection = await getChatsCollections();

	if (stage === QUERY_DATA.ADD_LOCATION) {
		const options = {
			reply_markup: {
				inline_keyboard: [
					...ADD_REVIEW_FORM_KEYBOARD,
					[REMOVE_LOCATION_BUTTON],
				],
			},
			parse_mode: 'HTML',
		};

		const locationLink = getLocationLink(location);

		const updateObj = {
			$set: {
				reviewLocation: location,
				reviewLocationLink: locationLink,
			}
		};

		await collection.updateOne({ chatId: id }, updateObj);

		const review = await getReview(id);

		const reviewText = getReviewText(review);

		await sendMessage(reviewText, options);
	}
};
