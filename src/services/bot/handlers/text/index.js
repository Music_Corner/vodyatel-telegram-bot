/* eslint-disable no-case-declarations */
import { sendBotMessage } from '../..';
import { getReviewMessageMarkup } from '../../../../common/botMesssages';
import { ENV } from '../../../../common/contsants/env';
import { ADD_LOCATION_BUTTON, ADD_REVIEW_FORM_KEYBOARD, CHAT_SUCCESS_RESPONSES_BY_STAGE, ERRORS_MESSAGES, MESSAGE_TEXTS, NAVIGATION, PAGINATION_BUTTONS, QUERY_DATA, SCENARIOS_COMMANDS } from '../../../../common/contsants/scenarios';
import { parseCarNumber, validateCarNumber, validateReviewText } from '../../../../common/helpers';
import ApiService from '../../../api';
import { logger } from '../../../logger';
import { getChatInstance, getChatsCollections, getReview, getReviewsText, getReviewText } from '../common';

export const handleBotText = async (message, match) => {
	const apiService = new ApiService();

	const { chat: { id, username } } = message
	const [messageText, query] = match;
	const sendMessage = sendBotMessage.bind(null, id);
	const { stage } = await getChatInstance(id);
	const collection = await getChatsCollections();
	const successResponse = CHAT_SUCCESS_RESPONSES_BY_STAGE[stage];

	let updateObj = {};
	let options = {};

	switch (stage) {
		case SCENARIOS_COMMANDS.CHECK:
			if (!validateCarNumber(messageText)) {
				await sendMessage(ERRORS_MESSAGES.CAR_NUMBER_VALIDATION);
				return;
			}

			await sendMessage(MESSAGE_TEXTS.LOADING);

			const parsedCarNumber = parseCarNumber(messageText);

			let items, count;

			try {
				const { items: _items, count: _count } = await apiService.getReviews(parsedCarNumber);

				items = _items;
				count = _count;
			} catch (e) {
				await sendMessage(ERRORS_MESSAGES.COMMON_RETRY);
			}

			if (!items) return;

			if (!items.length) {
				await sendMessage(ERRORS_MESSAGES.NO_REVIEWS_BY_NUMBER);
				return;
			}

			const reviewsText = getReviewsText(items);

			const { reviewId } = items[0];

			// eslint-disable-next-line camelcase
			const reply_markup = await getReviewMessageMarkup({
				...items[0],
				reviewId,
				chatId: id,
				page: 0,
				count,
			});

			options = {
				reply_markup,
				parse_mode: 'HTML',
			};

			await sendMessage(reviewsText, options);
			return;

		case SCENARIOS_COMMANDS.ADD_REVIEW:
			if (!validateCarNumber(messageText)) {
				await sendMessage(ERRORS_MESSAGES.CAR_NUMBER_VALIDATION);
				return;
			}

			updateObj = {
				$set: {
					stage: SCENARIOS_COMMANDS.ADD_REVIEW_TEXT,
					reviewCarNumber: messageText,
					reviewUserName: username,
				},
			};

			await collection.updateOne({ chatId: id }, updateObj);

			await sendMessage(successResponse);

			return;

		case SCENARIOS_COMMANDS.ADD_REVIEW_TEXT:
			if (!validateReviewText(messageText)) {
				await sendMessage(ERRORS_MESSAGES.TEXT_VALIDATION);
				return;
			}

			updateObj = {
				$set: {
					stage: SCENARIOS_COMMANDS.ADD_REVIEW_RATE,
					reviewText: messageText,
				},
			};

			await collection.updateOne({ chatId: id }, updateObj);

			await sendMessage(successResponse);

			return;

		case SCENARIOS_COMMANDS.ADD_REVIEW_RATE:
			updateObj = {
				$set: {
					reviewRate: messageText,
				},
			};

			await collection.updateOne({ chatId: id }, updateObj);

			const review = await getReview(id);

			const reviewText = getReviewText(review);

			options = {
				reply_markup: {
					inline_keyboard: [
						...ADD_REVIEW_FORM_KEYBOARD,
						[ADD_LOCATION_BUTTON],
					],
				},
				parse_mode: 'HTML',
			};

			await sendMessage(reviewText, options);
			return;

		default:
			return;
	}
}
