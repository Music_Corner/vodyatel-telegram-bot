import { sendBotMessage } from '../..';
import { ENV } from '../../../../common/contsants/env';
import { ERRORS_MESSAGES, SCENARIOS_COMMANDS } from '../../../../common/contsants/scenarios';
import ApiService from '../../../api';
import { logger } from '../../../logger';
import { getChatInstance } from '../common';
import { editBotMessageTextWithNewReviewData } from './common';

export const handleReviewsPageQuery = async (callBackData) => {
	const { data: queryData, id: callBackQueryId, message } = callBackData;
	// eslint-disable-next-line camelcase
	const { chat: { id }, message_id } = message;
	const sendMessage = sendBotMessage.bind(null, id);

	const chatData = await getChatInstance(id);

	const {
		carNumber,
		stage,
	} = chatData;

	const page = chatData[queryData];

	const apiService = new ApiService();

	let items;
	let	count;

	const limit = ENV.DEFAULT_LIMIT;
	try {
		// eslint-disable-next-line no-underscore-dangle
		const _carNumber = stage === SCENARIOS_COMMANDS.CHECK_LATEST ? undefined : carNumber;

		const { items: _items, count: _count } = await apiService.getReviews(_carNumber, page, limit);

		items = _items;
		count = _count;
	} catch (e) {
		logger.error(e);
		await sendMessage(ERRORS_MESSAGES.COMMON_RETRY);
	}

	if (!count || !items) return;

	const [review] = items;

	const { id: reviewId } = review;

	const newReviewData = {
		carNumber,
		count,
		page,
		reviewId,
		...review,
	};

	await editBotMessageTextWithNewReviewData(
		newReviewData,
		id,
		message_id,
		stage === SCENARIOS_COMMANDS.CHECK_LATEST,
		callBackQueryId,
	);
};
