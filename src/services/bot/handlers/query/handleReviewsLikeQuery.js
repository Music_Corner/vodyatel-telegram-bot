/* eslint-disable camelcase */
import { sendBotMessage } from '../..';
import { ERRORS_MESSAGES, SCENARIOS_COMMANDS } from '../../../../common/contsants/scenarios';
import ApiService from '../../../api';
import { getChatInstance } from '../common';
import { editBotMessageTextWithNewReviewData } from './common';

export const handleReviewsLikeQuery = async (callBackData) => {
	const { data: queryData, id: callBackQueryId, message } = callBackData;
	const apiService = new ApiService();
	const { chat: { id }, message_id } = message;
	const sendMessage = sendBotMessage.bind(null, id);

	const {
		stage,
		reviewId,
		...chatInstance
	} = await getChatInstance(id);

	let review;

	try {
		await apiService[`${queryData}Review`](reviewId, id);

		review = await apiService.getReview(reviewId);
	} catch (error) {
		await sendMessage(ERRORS_MESSAGES.COMMON_RETRY);
	}

	if (!review) return;

	const newReviewData = {
		...chatInstance,
		...review,
		reviewId,
	};

	await editBotMessageTextWithNewReviewData(
		newReviewData,
		id,
		message_id,
		stage === SCENARIOS_COMMANDS.CHECK_LATEST,
		callBackQueryId,
	);
};
