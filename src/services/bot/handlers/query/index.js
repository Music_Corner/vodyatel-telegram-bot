/* eslint-disable camelcase */
import { editBotMessageText, sendBotMessage } from '../..';
import { initBot } from '../../initBot';
import { ADD_LOCATION_BUTTON, ADD_REVIEW_FORM_KEYBOARD, QUERY_DATA, INITIAL_CHAT_STATE, NAVIGATION, SCENARIOS_COMMANDS, STATIC_COMANDS_RESPONSES, ERRORS_MESSAGES } from '../../../../common/contsants/scenarios';
import ApiService from '../../../api';
import { getChatsCollections, getReview, getReviewText } from '../common';
import { handleReviewsPageQuery } from './handleReviewsPageQuery';
import { handleReviewsLikeQuery } from './handleReviewsLikeQuery';

export const handleCallbackQuery = async (callbackData) => {
	const { data, message } = callbackData;
	const { message_id, chat: { id } } = message;
	const collection = await getChatsCollections();

	const sendMessage = sendBotMessage.bind(null, id);
	const bot = initBot();

	const apiService = new ApiService();

	let updateObj = {};
	let replyMarkup = null;

	const clearInlineKeyboard = async () => {
		await bot.editMessageReplyMarkup(null, { chat_id: id, message_id });
	};

	const updateChatCollection = async () => {
		await collection.updateOne({ chatId: id }, updateObj);
	};

	let review = await getReview(id);

	if (data.toLowerCase().includes('page')) {
		await handleReviewsPageQuery(callbackData);
	}

	if (data.toLowerCase().includes('like')) {
		await handleReviewsLikeQuery(callbackData);
	}

	switch (data) {
		case QUERY_DATA.OK:
			updateObj = {
				$set: {
					stage: SCENARIOS_COMMANDS.ADD_REVIEW_DONE,
				},
			};

			let reviewAddedSuccessfully = false;

			try {
				await apiService.addReview(review);

				reviewAddedSuccessfully = true;
			} catch (e) {
				await sendMessage(ERRORS_MESSAGES.COMMON_RETRY);
			}

			if (reviewAddedSuccessfully) {
				await updateChatCollection();

				await sendMessage(STATIC_COMANDS_RESPONSES[SCENARIOS_COMMANDS.ADD_REVIEW_DONE]);

				await clearInlineKeyboard();
			}

			break;

		case QUERY_DATA.CANCEL:
			updateObj = {
				$set: INITIAL_CHAT_STATE,
			};

			await updateChatCollection();

			await sendMessage(NAVIGATION.MAIN);
			await clearInlineKeyboard();
			break;

		case QUERY_DATA.EDIT:
			updateObj = {
				$set: {
					...INITIAL_CHAT_STATE,
					stage: SCENARIOS_COMMANDS.ADD_REVIEW,
				},
			};

			await updateChatCollection();

			await sendMessage(STATIC_COMANDS_RESPONSES[SCENARIOS_COMMANDS.ADD_REVIEW]);
			await clearInlineKeyboard();
			break;

		case QUERY_DATA.ADD_LOCATION:
			updateObj = {
				$set: {
					stage: QUERY_DATA.ADD_LOCATION,
				},
			};

			await updateChatCollection();

			await sendMessage(STATIC_COMANDS_RESPONSES[QUERY_DATA.ADD_LOCATION]);
			await clearInlineKeyboard();
			break;

		case QUERY_DATA.REMOVE_LOCATION:
			updateObj = {
				$set: {
					stage: SCENARIOS_COMMANDS.ADD_REVIEW_RATE,
					reviewLocation: null,
					reviewLocationLink: '',
				},
			};

			await updateChatCollection();

			replyMarkup = {
				inline_keyboard: [
					...ADD_REVIEW_FORM_KEYBOARD,
					[ADD_LOCATION_BUTTON],
				],
			};

			let reviewText = '';

			try {
				review = await getReview(id);
				reviewText = getReviewText(review);
			} catch (e) {
				await sendMessage(ERRORS_MESSAGES.COMMON_RETRY);
			}

			if (reviewText) {
				await editBotMessageText(reviewText, { chat_id: id, message_id, reply_markup: replyMarkup, parse_mode: 'HTML', });
			}

			break;

		default:
			break;
	}
};
