/* eslint-disable camelcase */
import { editBotMessageText } from '../..';
import { getReviewMessageMarkup } from '../../../../common/botMesssages';
import { getReviewsText } from '../common';

export const editBotMessageTextWithNewReviewData = async (
	review,
	chat_id,
	message_id,
	showEveryCarNumber,
	callBackQueryId,
) => {
	const {
		carNumber,
		count,
		page,
		likes,
		dislikes,
		reviewId,
	} = review;

	// eslint-disable-next-line camelcase
	const reply_markup = await getReviewMessageMarkup({
		carNumber,
		count,
		page,
		likes,
		dislikes,
		reviewId,
		chatId: chat_id,
	});

	const reviewsText = getReviewsText([review], page, showEveryCarNumber);

	await editBotMessageText(
		reviewsText,
		{ chat_id, message_id, reply_markup, parse_mode: 'HTML' },
		callBackQueryId,
	);
};
