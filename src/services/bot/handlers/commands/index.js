/* eslint-disable no-case-declarations */
import { sendBotMessage } from '../..';
import { getReviewMessageMarkup } from '../../../../common/botMesssages';
import { ERRORS_MESSAGES, MESSAGE_TEXTS, SCENARIOS_COMMANDS, STATIC_COMANDS_RESPONSES } from '../../../../common/contsants/scenarios';
import ApiService from '../../../api';
import { getChatsCollections, getReviewsText } from '../common';

export const handleBotCommands = async (message, match) => {
	const {
		chat: { id },
		text,
	} = message;

	const command = text.replace('/', '');
	const apiService = new ApiService();

	const sendMessage = sendBotMessage.bind(null, id);

	const collection = await getChatsCollections();

	await collection.update({ chatId: id }, { chatId: id, stage: command }, { upsert: true });

	switch (command) {
		case SCENARIOS_COMMANDS.CHECK_LATEST:
			await sendMessage(MESSAGE_TEXTS.LOADING);

			let items;
			let count;

			try {
				const { items: _items, count: _count } = await apiService.getReviews(undefined, 0, 1);

				items = _items;
				count = _count;
			} catch (e) {
				await sendMessage(ERRORS_MESSAGES.COMMON_RETRY);
			}

			if (!items.length) {
				await sendMessage(ERRORS_MESSAGES.NO_REVIEWS);
				return;
			}

			const reviewsText = getReviewsText(items, 0, true);

			const { id: reviewId } = items[0];

			// const filter = { chatId: id };

			// const updateDoc = {
			// 	$set: { carNumber: null },
			// };

			// await collection.update(filter, updateDoc);

			// eslint-disable-next-line camelcase
			const reply_markup = await getReviewMessageMarkup({
				...items[0],
				reviewId,
				chatId: id,
				page: 0,
				count,
			});

			const options = {
				reply_markup,
				parse_mode: 'HTML',
			};

			await sendMessage(reviewsText, options);

			break;

		default:
			await sendMessage(STATIC_COMANDS_RESPONSES[command]);

			break;
	}
};
