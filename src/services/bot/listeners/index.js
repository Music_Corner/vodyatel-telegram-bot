import { initBot } from '../initBot';
import { COMMANDS } from '../../../common/contsants/scenarios';
import { logger } from '../../logger';
import { handleBotCommands } from '../handlers/commands';
import { handleBotLocation } from '../handlers/location';
import { handleCallbackQuery } from '../handlers/query';
import { handleBotText } from '../handlers/text';

export const initListeners = () => {
	try {
		const bot = initBot();

		bot.onText(/^\/\w*/gm, async (message, match) => {
			const { chat: { id }, text } = message;

			try {
				logger.info(`recieved a message (command) from ${id} with text: ${text}`);
				await handleBotCommands(message, match);
			} catch (error) {
				logger.error(error);
			}
		});

		const nonCommandRegexp = /^[^\/].*/gm;
		bot.onText(nonCommandRegexp, async (message, match) => {
			const { chat: { id }, text } = message;

			try {
				logger.info(`recieved a message from ${id} with text: ${text}`);
				await handleBotText(message, match);
			} catch (error) {
				logger.error(error);
			}
		});

		bot.on('location', async (message) => {
			try {
				await handleBotLocation(message);
			} catch (error) {
				logger.error(error);
			}
		});

		bot.on('photo', (message) => {
			logger.info(message.photo);
		});

		/* Set comands list */
		try {
			bot.setMyCommands(COMMANDS).then((info) => {
				logger.info('Commands list set successfully!');
			});
		} catch (error) {
			logger.error(error);
		}

		bot.on('callback_query', async (callbackData) => {
			try {
				logger.info('recieved a callback query', callbackData);
				await handleCallbackQuery(callbackData);
			} catch (error) {
				logger.error(error);
			}
		});
	} catch (error) {
		logger.error(error);
	}
};
