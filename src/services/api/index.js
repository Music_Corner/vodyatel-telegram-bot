import initApi from '../../common/axios';
import { API_METHODS } from '../../common/contsants/api';
import { ENV } from '../../common/contsants/env';

export default class ApiService {
	/**
	 *
	 * @param {number} carNumber
	 * @param {number} page
	 * @param {number} limit
	 * @returns {Array}
	 */
	async getReviews(carNumber, page, limit = ENV.DEFAULT_LIMIT) {
		const { data: { data: reviews = [] } } = await initApi()
			.get(API_METHODS.REVIEWS, { params: { carNumber, page, limit } });

		return reviews;
	}

	async getReview(id) {
		const { data: { data } } = await initApi()
			.get(`${API_METHODS.REVIEWS}/${id}`, { params: { id } });

		return data;
	}

	/**
	 *
	 * @param {{
		* carNumber: String,
		* title: String,
		* text: String,
		* rate: Number
	 * }} review
	 */
	addReview(review) {
		return initApi().post(API_METHODS.REVIEWS, { review });
	}

	async getLocationString(location) {
		try {
			const { data: { data } } = await initApi().get(API_METHODS.LOCATION, { params: location });

			return data;
		} catch (error) {
			return `Адрес не распознан, ${error}`;
		}
	}

	/**
	 *
	 * @param {string} reviewId
	 * @param {string} userId
	 * @returns
	 */
	likeReview(reviewId, userId) {
		return initApi()
			.post(`${API_METHODS.REVIEWS}/${reviewId}${API_METHODS.LIKE}`, { userId });
	}

	/**
	 *
	 * @param {string} reviewId
	 * @param {string} userId
	 * @returns
	 */
	dislikeReview(reviewId, userId) {
		return initApi()
			.post(`${API_METHODS.REVIEWS}/${reviewId}${API_METHODS.DISLIKE}`, { userId });
	}
}
