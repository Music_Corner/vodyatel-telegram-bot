import { MongoClient } from 'mongodb';
import { ENV } from '../../common/contsants/env';

let mongoClient;
let connection = null;

const getMongoConnection = async () => {
	if (!mongoClient) {
		const options = {
			useUnifiedTopology: true,
			auth: {
				user: process.env.MONGO_USERNAME,
				password: process.env.MONGO_PASSWORD,
			},
		};

		mongoClient = new MongoClient(ENV.MONGO_DB_CONNECTION_URL, options);
	}

	if (!connection) {
		connection = await mongoClient.connect();
	}

	return connection;
};

export default getMongoConnection;
