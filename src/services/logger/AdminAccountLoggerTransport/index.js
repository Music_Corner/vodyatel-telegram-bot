import Transport from 'winston-transport';
import { initBot } from '../../bot/initBot';

export default class AdminAccountLoggerTransport extends Transport {
	log(info, callback) {
		try {
			setImmediate(() => {
				this.emit('logged', info);
			});

			initBot().sendMessage('402323254', JSON.stringify(info));

			callback();	
		} catch (error) {
			console.error(error);
		}
	}
}
