import winston, { format } from 'winston';
import AdminAccountLoggerTransport from './AdminAccountLoggerTransport';

const transports = [
	new winston.transports.File({ filename: 'error.log', level: 'error' }),
	new winston.transports.File({ filename: 'combined.log' }),
];

if (process.env.NODE_ENV === 'production') {
	transports.push(new AdminAccountLoggerTransport({}));
}

export const logger = winston.createLogger({
	format: format.combine(
		format.errors({ stack: true }),
		format.timestamp(),
		format.json(),
		format.metadata(),
	),
	defaultMeta: { service: 'user-service' },
	transports,
});

// if (process.env.NODE_ENV !== 'production') {
// logger.add(new winston.transports.Console({
// 	format: winston.format.simple(),
// }));
// }
