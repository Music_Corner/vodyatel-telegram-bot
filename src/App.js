import { initListeners } from './services/bot/listeners';
import { logger } from './services/logger';

const App = () => {
	logger.info('starting bot');

	initListeners();
};

export default App;
