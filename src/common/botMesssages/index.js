/* eslint-disable no-underscore-dangle */
import { getChatsCollections } from '../../services/bot/handlers/common';
import { ENV } from '../contsants/env';
import { LIKE_DISLIKE_BUTTONS, PAGINATION_BUTTONS } from '../contsants/scenarios';
import { isNullOrUndefined } from '../helpers';

export const getReviewMessageMarkup = async ({
	reviewId,
	chatId,
	carNumber,
	count,
	limit = ENV.DEFAULT_LIMIT,
	page,
	likes = 0,
	dislikes = 0,
}) => {
	const prevPage = page - 1 < 0 ? null : page - 1;
	const nextPage = limit * page + 1 < count ? page + 1 : null;

	const callBackData = {
		page,
		chatId,
		reviewId,
		carNumber,
		prevPage,
		nextPage,
		count,
	};

	const prevPageButton = !isNullOrUndefined(prevPage) && {
		...PAGINATION_BUTTONS.PREV,
		callback_data: 'prevPage',
		// callback_data: getCallBackData('page', { page: prevPage }),
	};

	const nextPageButton = !isNullOrUndefined(nextPage) && {
		...PAGINATION_BUTTONS.NEXT,
		callback_data: 'nextPage',
		// callback_data: getCallBackData('page', { page: nextPage }),
	};

	const likeButton = {
		...LIKE_DISLIKE_BUTTONS.LIKE,
		text: `${LIKE_DISLIKE_BUTTONS.LIKE.text}${likes}`,
		callback_data: 'like',
		// callback_data: getCallBackData('like'),
	};

	const dislikeButton = {
		...LIKE_DISLIKE_BUTTONS.DISLIKE,
		text: `${LIKE_DISLIKE_BUTTONS.DISLIKE.text}${dislikes}`,
		callback_data: 'dislike',
		// callback_data: getCallBackData('dislike'),
	};

	const getButtonsRow = (buttonsArray) => buttonsArray.filter(a => a);

	const collection = await getChatsCollections();

	const filter = { chatId };

	const updateDoc = {
		$set: callBackData,
	};

	await collection.update(filter, updateDoc);

	const options = {
		inline_keyboard: [
			getButtonsRow([
				prevPageButton,
				nextPageButton,
			]),

			[
				likeButton,
				dislikeButton,
			],
		],
	};

	return options;
};
