import { CAR_NUMBER_REGEXPS } from '../contsants/regexp';
import { COMMANDS } from '../contsants/scenarios';

export const parseRate = _rate => {
	let rate = Math.round((Number.isNaN(Number(_rate)) ? 0 : Number(_rate)));

	if (rate > 5) {
		rate = 5;
	}

	if (rate < 0) {
		rate = 0;
	}

	return rate;
};

export const parseCarNumber = (carNumber = '') => (`${carNumber}`).replace(/\s/g, '').toLowerCase();

export const validateCarNumber = (carNumber = '') => {
	const parsedNumber = parseCarNumber(carNumber);

	const testAnyNumber = Object.values(CAR_NUMBER_REGEXPS).find(regexp => regexp.test(parsedNumber));

	if (testAnyNumber) {
		return parsedNumber;
	}

	return false;
};

export const validateReviewText = (text = '') => text.length <= 350;

export const getLocationLink = ({ longitude, latitude }) => `http://www.google.com/maps/place/${latitude},${longitude}`;

export const getNavigation = (format = '') => COMMANDS.reduce((accum, { command: _command, description }, index) => {
	const postfix = index === Object.keys(COMMANDS).length - 1
		? '' : '\n';

	const formattedCommands = {
		HTML: `<strong>/${_command}</strong>`,
		Markdown: `*/${_command}*`,
	};

	const command = formattedCommands[format] || `/${_command}`;

	return `${accum}${command} - ${description}${postfix}`;
}, '');

export const isNullOrUndefined = (value) => value === undefined || value === null;
