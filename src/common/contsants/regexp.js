export const CAR_NUMBER_REGEXPS = {
	REGULAR_NUMBER_REGEXP: /^[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}$/ui,
	TAXI_NUMBER_REGEXP: /^[АВЕКМНОРСТУХ]{2}\d{3}(?<!000)\d{2,3}$/ui,
	TRAILER_NUMBER_REGEXP: /^[АВЕКМНОРСТУХ]{2}\d{4}(?<!0000)\d{2,3}$/ui,
	MOTO_REGEXP: /^\d{4}(?<!0000)[АВЕКМНОРСТУХ]{2}\d{2,3}$/ui,
	TRANSIT_REGEXP: /^[АВЕКМНОРСТУХ]{2}\d{3}(?<!000)[АВЕКМНОРСТУХ]\d{2,3}$/ui,
	OUT_TRANSPORT_REGEXP: /^Т[АВЕКМНОРСТУХ]{2}\d{3}(?<!000)\d{2,3}$/ui,
};
