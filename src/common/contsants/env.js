export const ENV = {
	DEV_ENV: 'development',
	PROD_ENV: 'production',
	IS_DEV() {
		return process.env.NODE_ENV === this.DEV_ENV;
	},

	API_BASE_URL: 'http://localhost:8081',
	MONGO_DB_CONNECTION_URL: 'mongodb://0.0.0.0:27017/',
	DEFAULT_LIMIT: 1,
};
