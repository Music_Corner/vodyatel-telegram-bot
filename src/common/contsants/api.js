export const API_METHODS = {
	REVIEWS: '/reviews',
	LOCATION: '/location',
	LIKE: '/like',
	DISLIKE: '/dislike',
};
