import { getNavigation } from '../helpers';

export const SCENARIOS_COMMANDS = {
	START: 'start',
	ADD_REVIEW: 'addreview',
	// ADD_REVIEW_TITLE: 'addreview/title',
	ADD_REVIEW_TEXT: 'addreview/text',
	ADD_REVIEW_RATE: 'addreview/rate',
	ADD_REVIEW_DONE: 'addreview/done',
	CHECK: 'check',
	CHECK_LATEST: 'checklatest',
};

export const COMMANDS = [
	{ command: SCENARIOS_COMMANDS.CHECK_LATEST, description: 'Топ свежих отзывов' },
	{ command: SCENARIOS_COMMANDS.CHECK, description: 'Посмотреть отзывы' },
	{ command: SCENARIOS_COMMANDS.ADD_REVIEW, description: 'Добавить отзыв' },
];

export const NAVIGATION = {
	MAIN: getNavigation('Markdown'),
	MAIN_NO_MARKDOWN: getNavigation(),
	MAIN_HTML_FORMAT: getNavigation('HTML'),
};

export const QUERY_DATA = {
	OK: 'addreview/submit',
	CANCEL: 'addreview/cancel',
	EDIT: 'addreview/edit',
	ADD_LOCATION: 'addreview/addlocation',
	REMOVE_LOCATION: 'addreview/removelocation',

	PAGE: 'check/page',
};

export const ADD_REVIEW_FORM_KEYBOARD_TEXT = {
	OK: 'Ок✅',
	CANCEL: 'Отмена❌',
	EDIT: 'Заполнить заново📝',
	ADD_LOCATION: 'Добавить местоположение📍',
	REMOVE_LOCATION: 'Удалить местоположение❌',
	ADD_PHOTOS: 'Добавить фото',
	REMOVE_PHOTOS: 'Удалить фото❌',
};

export const MESSAGE_TEXTS = {
	TYPE_CAR_NUMBER: 'Впиши гос номер автомобиля. К примеру:\n_е000ее55_',
	LOADING: 'Загрузка..🚥🚥🚥',
};

export const STATIC_COMANDS_RESPONSES = {
	[SCENARIOS_COMMANDS.START]: `Привет!🙌 Ты попал в *Водятел бот*! Оставляй и читай отзывы о водителях🚗\n\n\n${NAVIGATION.MAIN}`,
	[SCENARIOS_COMMANDS.CHECK]: `*Просмотр отзывов*\n\n${MESSAGE_TEXTS.TYPE_CAR_NUMBER}`,
	[SCENARIOS_COMMANDS.ADD_REVIEW]: `*Оставить отзыв*\n\n${MESSAGE_TEXTS.TYPE_CAR_NUMBER}`,
	[SCENARIOS_COMMANDS.ADD_REVIEW_DONE]: `*Отзыв успешно добавлен!*\n\n\n${NAVIGATION.MAIN}`,
	[QUERY_DATA.ADD_LOCATION]: '*Пришлите местоположение*\n\nВложения "📎" --> Местоположение "📍"\nНа карте укажите место и отправьте',
};

export const CHAT_SUCCESS_RESPONSES_BY_STAGE = {
	[SCENARIOS_COMMANDS.ADD_REVIEW]: '*Напишите текст отзыва*\nК примеру: _НСК 23:00. Чуть не сбил пешехода_',
	[SCENARIOS_COMMANDS.ADD_REVIEW_TEXT]: '*Поставьте водителю оценку*\nОт *0* до *5*',
};

export const ERRORS_MESSAGES = {
	COMMON: 'Что-то пошло не так..',
	COMMON_RETRY: 'Что-то пошло не так, попробуйте снова',
	NO_REVIEWS: `Отзывов пока нет =(\n\n\n${NAVIGATION.MAIN}`,
	NO_REVIEWS_BY_NUMBER: `*Отзывов по такому гос номеру не найдено*\nПопробуйте другой номер\n\n\n${NAVIGATION.MAIN}`,

	VALIDATION: '*Ошибка валидации*',
	get CAR_NUMBER_VALIDATION() {
		return `${this.VALIDATION}. Номер должен соответствовать _ГОСТу_\nК примеру: _е000ее55_. Так же допускаются пробелы между символами. *Буквы русские*`;
	},

	get TEXT_VALIDATION() {
		return `${this.VALIDATION}. Длина текста отзыва не должна превышать *350* символов`;
	},
};

export const PAGINATION_BUTTONS = {
	PREV: { callback_data: QUERY_DATA.PAGE, text: '⬅' },
	NEXT: { callback_data: QUERY_DATA.PAGE, text: '➡' },
};

export const LIKE_DISLIKE_BUTTONS = {
	LIKE: { callback_data: '/dislike', text: '👍' },
	DISLIKE: { callback_data: '/like', text: '👎' },
};

export const ADD_REVIEW_FORM_KEYBOARD = [
	[{
		callback_data: QUERY_DATA.CANCEL,
		text: ADD_REVIEW_FORM_KEYBOARD_TEXT.CANCEL,
	}, {
		callback_data: QUERY_DATA.OK,
		text: ADD_REVIEW_FORM_KEYBOARD_TEXT.OK,
	}],
	[{
		callback_data: QUERY_DATA.EDIT,
		text: ADD_REVIEW_FORM_KEYBOARD_TEXT.EDIT,
	}],
];

export const ADD_LOCATION_BUTTON = {
	callback_data: QUERY_DATA.ADD_LOCATION,
	text: ADD_REVIEW_FORM_KEYBOARD_TEXT.ADD_LOCATION,
};

export const REMOVE_LOCATION_BUTTON = {
	callback_data: QUERY_DATA.REMOVE_LOCATION,
	text: ADD_REVIEW_FORM_KEYBOARD_TEXT.REMOVE_LOCATION,
};

export const INITIAL_CHAT_STATE = {
	stage: '',
	reviewCarNumber: '',
	reviewText: '',
	reviewRate: '',
	reviewLocation: null,
	reviewLocationLink: '',
};
