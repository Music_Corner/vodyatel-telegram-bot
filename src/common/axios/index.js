import axios from 'axios';

import { ENV } from '../contsants/env';

const initApi = () => axios.create({
	baseURL: process.env.API_BASE_URL || ENV.API_BASE_URL,
	headers: { Authorization: `Bearer ${process.env.API_TOKEN}` },
});

export default initApi;
